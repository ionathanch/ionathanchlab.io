---
layout: page
title: CV
long_title: Curriculum Vitæ
sidebar_link: true
---

**📧mail**: <span>jcxz</span>@<span>seas.upenn.edu</span>
<br/>
**ORCID**: [0000-0003-0830-3180](https://orcid.org/0000-0003-0830-3180){:target="_blank"}

## Education

**University of Pennsylvania**, *Philadelphia, PA, USA*
<br/>
PhD in Computer and Information Science, _ongoing_
<br/>
Advisor: [Stephanie Weirich](https://www.cis.upenn.edu/~sweirich/)

**University of British Columbia**, *Vancouver, BC, Canada*
<br/>
MSc. in Computer Science, _November 2022_
<br/>
Advisor: [William J. Bowman](https://www.williamjbowman.com/)
<br/>
*Sized Dependent Types via Extensional Type Theory*
<br/>
[[Thesis](http://hdl.handle.net/2429/82209)] [[GitHub](https://github.com/ionathanch/msc-thesis)] [[Slides](https://docs.google.com/presentation/d/1gtLuA1I0Xgon1Em218aV_eBpx08Z0LGPIzG1hqbBVOk/)]

{% bibliography --template bibtex --query @*[key=STT] %}

**University of British Columbia**, *Vancouver, BC, Canada*
<br/>
BSc. Combined Honours in Computer Science and Physics, _May 2020_
<br/>
Advisor: William J. Bowman
<br/>
*Practical Sized Typing for Coq*
<br/>
[[Thesis](http://hdl.handle.net/2429/80455)] [[GitHub](https://github.com/ionathanch/coq/tree/dev)] [[Proposal](/assets/pdfs/bsc-proposal.pdf)]

{% bibliography --template bibtex --query @*[key=PSTC] %}

## Drafts and Publications

**Bounded First-Class Universe Levels in Dependent Type Theory**
<br/>
<mark>Jonathan Chan</mark> and Stephanie Weirich
<br/>
*Submitted to FSCD 2025*
<br/>
[[Preprint](/assets/pdfs/bfcul.pdf)]

**Consistency of a Dependent Calculus of Indistinguishability**
<br/>
[Yiyun Liu](https://electriclam.com/), <mark>Jonathan Chan</mark>, and Stephanie Weirich
<br/>
*Proceedings of the ACM on Programming Languages*, 9(POPL), January 2025
<br/>
[[PACMPL](https://doi.org/10.1145/3704843)] [[POPL 2025](https://popl25.sigplan.org/details/POPL-2025-popl-research-papers/7)] [[Artifact](https://doi.org/10.5281/zenodo.13930550)]

{% bibliography --template bibtex --query @*[key=DCOIω] %}

**Stratified Type Theory**
<br/>
<mark>Jonathan Chan</mark> and Stephanie Weirich
<br/>
*Accepted at ESOP 2025; presented at NJPLS 2023 @ Princeton University*
<br/>
[[arXiv](https://arxiv.org/abs/2309.12164)] [[GitHub](https://github.com/plclub/StraTT)] [[Slides](https://docs.google.com/presentation/d/1Zl7VH-ef0oUvvIgoDzsRgzVmMN8DkDeGEDqcMHsU6nQ/edit?usp=sharing)] [[ESOP 2025 draft](/assets/pdfs/stratt-esop.pdf)] [[FSCD 2024 draft](/assets/pdfs/stratt-fscd.pdf)] [[CPP 2024 draft](/assets/pdfs/stratt-fscd.pdf)]

{% bibliography --template bibtex --query @*[key=StraTT] %}

**Internalizing Extensions in Lattices of Type Theories**
<br/>
<mark>Jonathan Chan</mark>
<br/>
*CIS research qualifier @ UPenn*, 21 November 2024
<br/>
[[Report](https://github.com/ionathanch/rqual/blob/main/main.pdf)] [[Slides](https://docs.google.com/presentation/d/1F0HmoyO2VSz9OTLCJUbkkOt_-f-qnXa51oDXBfA4jjY/edit?usp=sharing)]

**Internalizing Indistinguishability with Dependent Types**
<br/>
Yiyun Liu, <mark>Jonathan Chan</mark>, [Jessica Shi](https://jwshii.github.io/), and Stephanie Weirich
<br/>
*Proceedings of the ACM on Programming Languages*, 8(POPL), January 2024
<br/>
[[PACMPL](https://doi.org/10.1145/3632886)] [[POPL 2024](https://popl24.sigplan.org/details/POPL-2024-popl-research-papers/46)] [[Artifact](https://doi.org/10.1145/3580424)]

{% bibliography --template bibtex --query @*[key=DCOI] %}

**Is Sized Typing for Coq Practical?**
<br/>
<mark>Jonathan Chan</mark>, Yufeng (Michael) Li, and William J. Bowman
<br/>
*Journal of Functional Programming*, 33(e1), January 2023
<br/>
[[JFP](https://doi.org/10.1017/S0956796822000120)] [[ICFP 2023](https://icfp23.sigplan.org/details/icfp-2023-papers/39)] [[arXiv](https://arxiv.org/abs/1912.05601)] [[GitHub](https://github.com/ionathanch/coq/releases/tag/V8.12%2Bsized-jfp)] [[Artifact](https://doi.org/10.5281/zenodo.5661975)] [[POPL 2021 draft](/assets/pdfs/pstc-popl2021.pdf)] [[CPP 2020 draft](/assets/pdfs/pstc-cpp2020.pdf)]

{% bibliography --template bibtex --query @*[key=ISTCP] %}

**Towards a Syntactic Model of Sized Dependent Types**
<br/>
<mark>Jonathan Chan</mark>
<br/>
*3<sup>rd</sup> place finalist at the [Student Research Competition](https://popl22.sigplan.org/track/POPL-2022-student-research-competition) @ [POPL 2022](https://popl22.sigplan.org/)*
<br/>
[[Abstract](/assets/pdfs/src2022-abstract.pdf)] [[Video + Slides](https://popl22.sigplan.org/details/POPL-2022-student-research-competition/1)] [[Slides (draft)](/assets/pdfs/sized-types-syntactic-model.pdf)]

{% bibliography --template bibtex --query @*[key=SRC] %}

## Nonacademic

**ergonomics of PL metatheory in Lean**
<br/>
presented at PLClub @ UPenn
<br/>
[[7 June 2024](https://docs.google.com/presentation/d/1SQGnHZ8yC-Ane6UgNmpp9jvV3GBsOMc8FOMvneFZnrM/)]

**quizzo**
<br/>
[[12 June 2023](/assets/pdfs/trivia-2023-06-12.pdf)] [[13 March 2023](/assets/pdfs/trivia-2023-03-13.pdf)]

**[Silence of the Vowels](https://www.cs.ubc.ca/~udls/showTalk.php/2021-10-22)**
<br/>
presented at [UDLS @ UBC](https://www.cs.ubc.ca/~udls)
<br/>
[[22 October 2021](https://docs.google.com/presentation/d/1u6xm3sfCZPGUpSFotxbgBRPE-NBxJrjmTvuSAfUprw4/)]

## Teaching Assistantships

### University of Pennsylvania

| Course   | Title | Term |
| -------- | ----- | ---- |
| CIS 5520 | Advanced Programming | [Fall 2024](https://www.seas.upenn.edu/~cis5520/24fa/), [Fall 2023](https://www.seas.upenn.edu/~cis5520/23fa/) |

### University of British Columbia

| Course   | Title | Term |
| -------- | ----- | ---- |
| CPSC 311 | Definition of Programming Languages | [2021 Winter 1](https://www.students.cs.ubc.ca/~cs-311/2021W1/), [2020 Winter 1](https://www.students.cs.ubc.ca/~cs-311/2020W1/) |
| CPSC 312 | Functional and Logic Programming | [2021 Winter 2](https://www.cs.ubc.ca/~poole/cs312/2021/) |
| CPSC 303 | Numerical Approximation and Discretization | [2020 Winter 2](https://www.cs.ubc.ca/~jf/courses/303.S2020/index.html) |

## Conferences

| Name | Location | Role |
| ---- | -------- | ---- |
| [ESOP 2025](https://etaps.org/2025/conferences/esop/) | Hamilton, Ontario, Canada | Presenter |
| [POPL 2025](https://popl25.sigplan.org/)              | Denver, Colorado, USA | Student volunteer |
| [POPL 2024](https://popl24.sigplan.org/)              | London, England, UK | Student volunteer |
| [NJPLS Nov 2023](https://njpls.org/nov2023.html)      | Princeton, New Jersey, USA | Presenter |
| [ICFP 2023](https://icfp23.sigplan.org/)              | Seattle, Washington, USA    | Student volunteer & presenter |
| [OPLSS 2022](https://www.cs.uoregon.edu/research/summerschool/summer22/) | Eugene, Oregon, USA | Attendee |
| [POPL 2022](https://popl22.sigplan.org/)              | virtual | Attendee |
| [OPLSS 2021](https://www.cs.uoregon.edu/research/summerschool/summer21/) | virtual | Attendee |
| [POPL 2021](https://popl21.sigplan.org/)              | virtual | Student volunteer |
| [ICFP 2020](https://icfp20.sigplan.org/)              | virtual | Student volunteer |
| [POPL 2020](https://popl20.sigplan.org/)              | New Orleans, Louisiana, USA | Student volunteer |
| [PLISS 2019](https://pliss2019.github.io/)            | Bertinoro, Italy | Attendee |

## Other Service and Work

<!--
**Department of Computer and Information Science**, UPenn, *Philadelphia, PA, USA*
<br/>
Teaching Assistant <small>([CIS 5520: Advanced Programming](https://www.seas.upenn.edu/~cis5520/23fa/))</small>
<br/>
*September – December 2023*
* Held weekly office hours
* Graded homeworks, quizzes, and group projects

**Department of Computer Science**, UBC, *Vancouver, BC, Canada*
<br/>
Teaching Assistant <small>([CPSC 311: Definition of Programming Languages](https://www.students.cs.ubc.ca/~cs-311/2021W1/))</small>
<br/>
*September – December 2021*
* Held weekly tutorial sessions and office hours
* Set up autograding for and graded assignments and exams
* Invigilated midterm and final exams

**Department of Computer Science**, UBC, *Vancouver, BC, Canada*
<br/>
Teaching Assistant <small>([CPSC 312: Functional and Logic Programming](https://www.cs.ubc.ca/~poole/cs312/2021/))</small>
<br/>
*January – April 2021*
* Held semiweekly office hours
* Graded group projects and midterm/final exams

**Department of Computer Science**, UBC, *Vancouver, BC, Canada*
<br/>
Teaching Assistant <small>([CPSC 311: Definition of Programming Languages](https://www.students.cs.ubc.ca/~cs-311/2020W1/))</small>
<br/>
*September – December 2020*
* Held biweekly tutorial sessions and weekly office hours
* Graded assignments and midterm/final exams
* Helped with administering and invigilating exams

**Department of Computer Science**, UBC, *Vancouver, BC, Canada*
<br/>
Teaching Assistant <small>([CPSC 303: Numerical Approximation and Discretization](https://www.cs.ubc.ca/~jf/courses/303.S2020/index.html))</small>
<br/>
*January – April 2020*
* Graded weekly assignments and held weekly office hours

-->

**[types.pl](https://types.pl)**, *Mastodon instance*
<br/>
Coädministrator and comoderator with [@haskal](https://types.pl/@haskal) (and formerly [Tulip](https://www.bicompact.space/))
<br/>
*23 October 2020 – present*

**[PLClub](https://www.cis.upenn.edu/~plclub/)**, University of Pennsylvania, *Philadelphia, PA, USA*
<br/>
Coörganizer with [Cassia Torczon](https://cassiatorczon.github.io/)
<br/>
*November 2023 – December 2024* 

**Digital Health Innovation Lab**, BC Children's Hospital, *Vancouver, BC, Canada*
<br/>
Software Programmer Co-op
<br/>
*May – August 2018*
* Contributed to [LambdaNative](https://github.com/part-cw/lambdanative), an open-source cross-platform app framework written in Gambit Scheme
* Added features to LambdaNative apps [RRate](https://github.com/part-cw/LNhealth) and [ShareVitalSigns](https://github.com/part-cw/sharevitalsigns)
* Cared for and nurtured the avocado seed growing on the windowsill in the lunch room

**Visier Inc.**, *Vancouver, BC, Canada*
<br/>
Software Developer Co-op
<br/>
*May – December 2017*
* Improved developer tools and workflows in the Frameworks and Enablement team
* Created a proof-of-concept for migrating codebase from Mercurial to Git and designed a custom Gitflow workflow
* Incorporated automated endpoint documentation generation in Scala backend codebase
* Helped improve Angular web app compile/load times and modularity

**Paragon Testing Enterprises**, *Vancouver, BC, Canada*
<br/>
Junior Software Developer and Quality Assurance Intern
<br/>
*January – August 2016*
* Wrote a pilot test delivery application in WinForms for R&D to test and validate the design of the new CAEL computerized exam
* Refactored web app test scripts
* Participated in Paragon's volleyball team for the Broadway Tech Centre's volleyball tournament

<!--

**Access and Diversity**, UBC, *Vancouver, BC, Canada*
<br/>
Notetaker
<br/>
*May – August 2015*
* Took lecture and textbook notes for PSYC 309A (Cognitive Processes) and PSYC 350A (Human Sexuality)

**Nikkei National Museum and Cultural Centre**, *Burnaby, BC, Canada*
<br/>
Volunteer Ambassador and Special Events Volunteer
<br/>
*September 2012 – June 2014*
* Welcomed visitors at front desk and various administrative duties
* Helped set up and take down equipment during various special events

**Burnaby Newsleader**, *Burnaby, BC, Canada*
<br/>
Paper delivery route
<br/>
*June 2013 – June 2014*
* Delivered the local paper every Wednesday and Friday. There isn't much to say about this. Sadly, they shut down in September of the following year :(
-->

## Scholarships and Awards

**George M. Ball, Jr. Fellowship** (UPenn), *2022*
<br/>
<small>Donor-named fellowships like these provide a one-time $3,000 award, which will be offered to you in the middle of your first semester upon enrollment.  This honor and award is in recognition of your outstanding academic accomplishments and research potential.</small>

**British Columbia Graduate Scholarship (declined)** (UBC), *2022*
<br/>
<small>The province of British Columbia has funded BC Graduate Scholarships (BCGS) in any field of study, with emphasis on research in science, technology, engineering and mathematics (STEM) fields, and support for Indigenous students. The scholarships of $15,000 will be awarded by graduate programs and disciplinary Faculties.</small>

**Canada Graduate Scholarship – Master's**, *2021*
<br/>
[[Outline of proposed research](/assets/pdfs/cgs-m-proposal.pdf)]
<br/>
<small>The objective of the Canada Graduate Scholarships – Master’s (CGS M) program is to help develop research skills and assist in the training of highly qualified personnel by supporting students who demonstrate a high standard of achievement in undergraduate and early graduate studies.</small>

[**CRA Outstanding Undergraduate Researcher Award Honourable Mention**](https://cra.org/about/awards/outstanding-undergraduate-researcher-award/#2020), *2020*
<br />
[[Research summary](/assets/pdfs/bsc-summary.pdf)]
<br/>
<small>This award program recognizes undergraduate students in North American colleges and universities who show outstanding potential in an area of computing research.</small>

**Dean of Science Scholarship** (UBC), *2018, 2015*
<br/>
<small>The Dean of Science Scholarship honours the most promising science undergraduate students and are made on the recommendation of the Faculty.</small>

**Trek Excellence Scholarship for Continuing Students** (UBC), *2018, 2016, 2015*
<br/>
<small>Trek Excellence Scholarships are offered every year to students in the top 5% of their undergraduate year, faculty, and school.</small>

**Charles and Jane Banks Scholarship** (UBC), *2019, 2017*
<br/>
<small>Scholarships are awarded on the recommendation of the Faculty of Science, to students who have completed at least one year, and are continuing in an undergraduate program.</small>

**J. Fred Muir Memorial Scholarship in Science** (UBC), *2016*
<br/>
<small>The awards are offered to students in the Faculty of Science on the recommendation of the Faculty.</small>

<!--
**Golden Key International Honour Society invitation (declined)**, *2019, 2015*
<br/>
<small>This is a scam for rich people.</small>
-->