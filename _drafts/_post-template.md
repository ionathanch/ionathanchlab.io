---
layout: post
title: "Post Template"
long_title: "This shows up only in the full post"
tab_title: "This shows up in the browser tab title"
excerpt_separator: "<!--more-->"
katex: true
tags:
  - post
categories:
  - templates
---

Including an image:

```html
{% include image.html
           img="assets/images/image.png"
           title="hovertext"
           caption="caption"
           url="https://ionathan.ch" %}
```

Escaping Liquid rendering:

```html
{% raw %}
{% include image.html
           caption="This include won't be rendered by Jekyll." %}
{% endraw %}
```

Linking to another post:

```markdown
[part 2]({{ site.baseurl }}{ % post_url yyyy-mm-dd-part-1 % })
```

Using LaTeX with `kramdown-math-katex`:

```latex
This is inline:
$$\int_{\partial \Omega} \omega = \int_{\Omega} d\omega$$

Adding `\displaystyle` puts the LaTeX on its own line:
$$\displaystyle \int_{\partial \Omega} \omega = \int_{\Omega} d\omega$$
```
