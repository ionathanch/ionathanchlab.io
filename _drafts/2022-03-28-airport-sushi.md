---
layout: post
title: "What's in my Airport Sushi?"
excerpt_separator: "<!--more-->"
tags:
  - food
  - sushi
  - travel
---

While travelling I got sushi from a shop in the airport.
It was okay. The ingredients list, though, was curiously long,
and while waiting for boarding I had time to inspect it.
Here's a photo of the original label with the ingredients.

<!-- more -->

If you read it carefully you'll find that there's two (!) levels
of nesting in this list, which makes it a little hard to parse.
Here it is again, now as a properly-formatted nested list.

```
- seasoned rice
  - water
  - rice
  - sushi seasoning
    - sugar
    - water
    - distilled vinegar
    - rice vinegar
    - salt
    - brown sugar
  - trehalose
- imitation crab meat
  - water
  - surimi
    - alaska pollock
    - egg white powder
    - sorbitol
    - sodium phosphates
  - wheat starch
  - modified tapioca starch
  - sugar
  - salt
  - palm oil
  - crab extract
    - soy
  - crab flavour
  - colour
  - calcium carbonate
- mayonnaise
  - canola oil
  - water
  - egg
  - vinegar
  - salt
  - sugar
  - spices
  - concentrated lemon juice
  - calcium disodium edta
- cucumber
- pickled ginger
  - ginger water
  - sugar
  - salt
  - citric acid
  - malic acid
  - acetic acid
  - vinegar
- soy sauce
  - water
  - wheat
  - soybeans
  - salt
  - sugar
  - brewing starter
    - aspergillus sojae
- chili sauce
  - chili
  - sugar
  - salt
  - garlic
  - disilled vinegar
  - potassium sorbate
  - sulphite
  - xanthan gum
- wasabi flavoured pasted
  - water
  - horseradish
  - mustard
  - corn flour
  - corn starch
  - citric acid
  - ascorbic acid
  - colour
- seaweed
- shichimi
  - orange peel
  - red pepper
  - black sesame
  - white sesame
  - seaweed
  - japanese pepper
```

It's a very long list.
It's a little surprising how many of these things have sugar in
them—why does the rice have sugar?
What I'm most interested in, though, is *what* some of these things are
and *why* they're in there.

## Sugars and Sugar Alcohols

(trehalose, sorbitol)

## Acids

(citric, malic, ascetic, ascorbic)

## Preservatives

(calcium carbonate, calcium disodium edta, potassium sorbate, sulphite)

## Starches and Gums

(xanthan gum, wheat starch, modified tapioca starch)