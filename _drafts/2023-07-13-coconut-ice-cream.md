---
layout: post
title: "No-Churn Coconut Ice Cream"
excerpt_separator: "<!--more-->"
tags:
  - ice cream
  - coconut
---

If you're familiar with the mechanics of no-churn ice creams,
this will sound like the natural extrapolation to coconut-based components:
condensed coconut milk, whipped coconut milk, sugar,
and a bit of grated coconut.
You won't need an ice cream machine, but you will need a stove, a mixer,
and about a days' waiting time.

<!--more-->

<style>
details {
  margin-top: inherit;
}
ul {
  list-style: none;
  padding-left: 1rem;
}
@media (min-width: 49rem) {
  .image-wrapper {
    width: 50%;
  }
}
.image-wrapper img {
  width: 85%;
}
</style>

### Ingredients

* <details>
    <summary>2 cans coconut milk (2 × 400 mL/14 fl oz)</summary>
    I recommend Aroy-D, which has basically no coconut water to separate out.
    Use cans that <strong>do not</strong> have any gums or thickener in them.
  </details>
* <details>
    <summary>½ cup sugar (125 mL)</summary>
    You could probably use coconut sugar for the Full Coconut Experience.
    It shouldn't make a difference for the condensed coconut milk,
    but it might be a little heavy for the whipped coconut milk.
    I'll report back once I've tried it.
  </details>
* <details>
    <summary>A couple tablespoons grated coconut</summary>
    Most dictionaries define "a couple" as two or more.
    I didn't measure it out and just grabbed what looked reasonable,
    which I would estimate amounted to 3 tbsp.
  </details>

### Equipment

* <details>
    <summary>A small pot, a stove, and a long stirring spoon</summary>
    <p>I used a metal spoon, which was kind of a poor choice, but I survived.</p>
  </details>
* <details>
    <summary>An electric mixer, a mixing bowl, and a rubber spatula</summary>
    <p>How did they make whipped cream before electric or mechanical mixers?
    You're gonna have a rough time trying to whip with a whisk.
    Contrarily, I didn't have a spatula, and made a mess scraping with a spoon,
    which I don't recommend.</p>
  </details>
* <details>
    <summary>A loaf pan or similarly-shaped wide, flat 3½-cup container</summary>
    Wider container means easier scooping.
  </details>
* <details>
    <summary>A ¼ measuring cup</summary>
    Look, I'm only being this specific because I didn't realize I was missing equipment
    after my housemate took the measuring cups in the divorce (moved out after lease was up).
  </details>

### Condensed coconut milk

1. <details>
     <summary>Add 1 can coconut milk and ¼ cup sugar to the pot.</summary>
     The particular sweetener doesn't matter too much here,
     but I don't know how much you might use in its place.
   </details>
2. <details>
     <summary>On low heat, bring to a simmer while stirring continuously.</summary>
     Don't stop stirring! The bottom <i>will</i> burn! Learn from my mistakes!
   </details>
3. <details>
     <summary>Once it has thickened and condensed down to about half the volume, let cool and chill in the fridge.</summary>
     This takes about half an hour. Feel free to zone out,
     because it'll be easier to tell that it's done when you zone back in
     and are surprised at how thick it suddenly is.
     <details>
       <summary>CW lewd</summary>
       When it starts looking like cum, you're good.
     </details>
   </details>

### Coconut ice cream

1. <details>
     <summary>Chill 1 can of coconut milk in the fridge until cold;
     chill the mixing bowl in the freezer until very cold.</summary>
     Chilling the can separates the coconut cream from the coconut water.
     If you want a more precise time estimate go use Newton's Law of Cooling or something.
2. <details>
     <summary>Scoop out the coconut cream in the top of the can into the mixing bowl,
     leaving behind the coconut water.</summary>
     If you use Aroy-D, there'll be barely any water at the bottom,
     so you can just dump the whole can in.
     Otherwise, do what you want with the water; we won't need it.
     You could drink it but that seems kind of gross.
   </details>
3. <details>
     <summary>Add in ¼ cup sugar while beating the coconut cream with the mixer on high until whipped.</summary>
     You'll know it when you see it.
     Recipes always say "stiff peaks" but that doesn't make much sense to me.
     You'll be standing there whipping for fifteen minutes panicking
     about how nothing's different until finally it looks different.
     If you swirl the bowl, it'll sort of slide around in a big lump
     instead of spreading around like a liquid.
   </details>
4. <details>
     <summary>Fold in the condensed coconut milk, then the grated coconut.</summary>
     Again, I didn't have a spatula, so I just stirred in spoonfuls of the condensed coconut milk until homogenous,
     then used the mixer on low for a few seconds,
     and did the same with the grated coconut.
   </details>
5. <details>
     <summary>Pour into the loaf pan and chill in the freezer overnight.</summary>
     This is the part where the spatula really comes in handy to scrape the sides of the bowl.
   </details>

### Gallery

<div style="display: flex; flex-wrap: wrap; margin-top: 1.5rem;">
{% include image.html
           img="assets/images/coconut-ice-cream/simmering.jpg"
           title="A pot of coconut milk simmering and the can being held in front of it"
           caption="You'll notice I stopped stirring to take this photo. Don't do that." %}

{% include image.html
           img="assets/images/coconut-ice-cream/condensed.jpg"
           title="A thick white creamy fluid in a mug"
           caption="Condensed coconut milk is viscous and goopy :)" %}

{% include image.html
           img="assets/images/coconut-ice-cream/cream.jpg"
           title="A metal mixing bowl with coconut cream"
           caption="Coconut cream is pretty thick when cold" %}

{% include image.html
           img="assets/images/coconut-ice-cream/whipped.jpg"
           title="A metal mixing bowl with whipped coconut cream and the mixer beside it"
           caption="I guess that's pretty stiff?" %}

{% include image.html
           img="assets/images/coconut-ice-cream/folded.jpg"
           title="A metal mixing bowl with a lumpy coconut ice cream mixture and the mixer beside it"
           caption="You just fold it in." %}

{% include image.html
           img="assets/images/coconut-ice-cream/poured.jpg"
           title="A wide glass container "
           caption="The hardest part is the waiting" %}
</div>