---
layout: post
title: "Film Photorolls"
excerpt_separator: "<!--more-->"
tags:
  - film photography
category:
  - Miscellany
---

<small><i>This was originally [posted on cohost](https://cohost.org/ionchy/post/4915745-film-photorolls).</i></small>

<style>
td {
  text-align: left;
}
a, td:has(> sup) {
  white-space: nowrap;
}
</style>

<!--more-->

| roll | number | date       | film                        | camera                | lab                    | albums |
| ---- | ------ | ---------- | --------------------------- | --------------------- | ---------------------- | ------ |
| 17   | 6155   | 2024-12-17 | Kodak ProImage 100          | Minolta Hi-Matic AF2  | PhotoLounge            | [https://flic.kr/s/aHBqjBVpFS](https://flic.kr/s/aHBqjBVpFS) |
| 16   | 2909   | 2024-11-05 | Kodak ColorPlus 200         | Minolta Hi-Matic AF2  | PhotoLounge            | [https://flic.kr/s/aHBqjBQGDn](https://flic.kr/s/aHBqjBQGDn) |
| 15   | 1729   | 2024-10-23 | Kodak Gold 200              | Minolta Hi-Matic AF2  | PhotoLounge            | [https://flic.kr/s/aHBqjBPje1](https://flic.kr/s/aHBqjBPje1) |
| 14   | 7519   | 2024-09-27 | Konica Minolta VX 100 Super | Canon Snappy 50       | PhotoLounge            | [https://flic.kr/s/aHBqjBMXzx](https://flic.kr/s/aHBqjBMXzx) <br/> [https://flic.kr/s/aHBqjBMXCy](https://flic.kr/s/aHBqjBMXCy) |
| 13   | 6416   | 2024-09-16 | CatLABS X Film 100          | Canon Sure Shot AF35M | PhotoLounge            | [https://flic.kr/s/aHBqjBMXjh](https://flic.kr/s/aHBqjBMXjh) |
| 12   | 5078   | 2024-08-16 | Wolfen NC400                | Canon Snappy 50       | Kerrisdale Cameras     | [https://flic.kr/s/aHBqjBMX8L](https://flic.kr/s/aHBqjBMX8L) |
| 11   | 0115   | 2024-07-08 | Wolfen NC500                | Canon Sure Shot AF35M | PhotoLounge            | [https://flic.kr/s/aHBqjBMSaG](https://flic.kr/s/aHBqjBMSaG) <br/> [https://flic.kr/s/aHBqjBMSaM](https://flic.kr/s/aHBqjBMSaM) |
| 10   | 5628   | 2024-06-10 | Fuji 200                    | Canon Sure Shot AF35M | PhotoLounge            | [https://flic.kr/s/aHBqjBMRg3](https://flic.kr/s/aHBqjBMRg3) |
| 09   | 0804   | 2024-04-15 | CatLABS X Film 100          | Canon Snappy 50       | PhotoLounge            | [https://flic.kr/s/aHBqjBMX7p](https://flic.kr/s/aHBqjBMX7p) <br/> [https://flic.kr/s/aHBqjBMX7j](https://flic.kr/s/aHBqjBMX7j) |
| 08   | 7763   | 2024-04-01 | Harman Phoenix 200          | Canon Snappy 50       | PhotoLounge            | [https://flic.kr/s/aHBqjBMR5k](https://flic.kr/s/aHBqjBMR5k) |
| 07   | 5756   | 2024-03-05 | Fuji 200                    | Canon Snappy 50       | PhotoLounge            | [https://flic.kr/s/aHBqjBMR5v](https://flic.kr/s/aHBqjBMR5v) <br/> [https://flic.kr/s/aHBqjBMR5q](https://flic.kr/s/aHBqjBMR5q) |
| 06   | 4861   | 2024-02-19 | Fuji 200                    | Kodak VR35 K300       | PhotoLounge            | [https://flic.kr/s/aHBqjBMMyt](https://flic.kr/s/aHBqjBMMyt) |
| 05   | 4089   | 2024-02-05 | Flic Film Elektra 100       | Kodak VR35 K300       | PhotoLounge            | [https://flic.kr/s/aHBqjBMMyy](https://flic.kr/s/aHBqjBMMyy) |
| 04   | 2078   | 2024-01-09 | Flic Film UltraPan 400      | Kodak VR35 K300       | Kerrisdale Cameras[^1] | [https://flic.kr/s/aHBqjBMMyD](https://flic.kr/s/aHBqjBMMyD) |
| 03   | 0194   | 2023-12-28 | Kodak Ultramax 400          | Kodak VR35 K300       | Kerrisdale Cameras[^1] | [https://flic.kr/s/aHBqjBMMyJ](https://flic.kr/s/aHBqjBMMyJ) |
| 02   | 0996   | 2023-12-13 | Kodak Gold 200              | Kodak VR35 K300       | PhotoLounge            | [https://flic.kr/s/aHBqjBMMyP](https://flic.kr/s/aHBqjBMMyP) |
| 01   | 7944   | 2023-11-27 | Kodak UltraMax 400          | Kodak Star 275        | PhotoLounge            | [https://flic.kr/s/aHBqjBMMyU](https://flic.kr/s/aHBqjBMMyU) |
| 00   | 6847   | 2023-11-07 | Fuji 200                    | Kodak Star 275        | PhotoLounge            | [https://flic.kr/s/aHBqjBMMyZ](https://flic.kr/s/aHBqjBMMyZ) |

[^1]: Rescanned at Indie Photo Lab.