---
layout: post
title: "An Evaluation of Error Diffusion Dithering with Negative Kernel Weights"
excerpt_separator: "<!--more-->"
tags:
  - dithering
---

The 2014 paper "Kernel weights optimization for error diffusion halftoning method" by Victor Fedoseev [1]
finds error diffusion dithering kernels with three, four, and 14 values by optimizing
for weighted signal-to-noise ratio over a search space of up to 3-by-5 kernels,
dithering on five fixed test images and verified over ten thousand test images from a database.
Unusually, they allow for kernel values with *negative* weights,
which are present in the four- and 14-value optimal kernels found.
However, these kernels produce extra unwanted noise on some input images and target palettes
when compared to other kernels without negatives weights in the literature.
In this post, I construct input images that produce additional noise,
and recommend situations in which *not* to use Fedoseev's negative-weight kernels.

<!--more-->

## Introduction

### Quantization

### Error Diffusion Dithering

### Related Work

## Evaluation of Fedoseev Kernels

In the following, I dither images using [`didder`](https://github.com/makeworld-the-better-one/didder).
It provides several well-known error diffusion dithering methods, including Floyd–Steinberg,
as well as the ability to use a custom diffusion kernel encoded as 2-dimensional JSON arrays.
`fedoseev-3.json`, `fedoseev-4.json`, and `fedoseev-12.json` refer to the following kernels from [1]:

```json
[[     0,      0, 0.4473],
 [0.1654, 0.3872,      0]]

[[     0,      0,   0.5221],
 [0.1854, 0.4689,        0],
 [     0,       0, -0.1763]]

[[      0,       0,      0,  0.5423,  0.0533],
 [ 0.0246,  0.2191, 0.4715, -0.0023, -0.1241],
 [-0.0065, -0.0692, 0.0168, -0.0952, -0.0304]]
```

I use a monochrome palette as well as the standard web-safe palette of 216 colours.
The web-safe palette as a space-separated list of hex values can be found [here](remember to link).

### Unbounded Error Propagation

The first test image is a 512 px × 512 px square of white with a 64 px wide stripe of grey #7F7F7F on the left.
The monochrome dithered images are generated using the following commands:

```sh
# Floyd-Steinberg
didder -i stripe.png -o stripe-floyd-steinberg.png -p "black white" edm FloydSteinberg
# Fedoseev 3/4/12
didder -i stripe.png -o stripe-fedoseev-{3,4,12}.png -p "black white" edm "fedoseev-{3,4,12}.json"
```

[remember to insert images here]

The original image, dithering using Floyd–Steinberg, and dithering using the three Fedoseev kernels are shown above.
In the Floyd–Steinberg and Fedoseev-3, the stripe is dithered in black and white to simulate the grey,
while the white area is left untouched.
On the other hand, the Fedoseev-4 and Fedoseev-12 ditherings produce trapezoids of noise along the white area.
The slope of the top noise boundary is larger in Fedoseev-4 than in Fedoseev-12, so the latter covers a larger area.

These areas of noise are caused by the negative weights.
Whereas with positive weights the error propagated right quickly diminishes to 0,
