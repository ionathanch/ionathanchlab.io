.PHONY: all build install help

all:
	bundle exec jekyll serve

build:
	bundle exec jekyll build

install:
	gem install bundler
	bundle install
	rougify style thankful_eyes > assets/css/syntax.css

bundle:
	rm Gemfile.lock
	bundle install

help:
	@echo "!! Ensure that you have the correct version of Ruby installed."
	@echo "!! Check https://pages.github.com/versions/ for the required version."
	@echo ""
	@echo "make [all]      Build and serve the site"
	@echo "make build      Build the site only"
	@echo "make install    Install Bundler and all Gems"
	@echo "make bundle     Remove Gemfile.lock and reinstall all Gems"
	@echo "make help       Display this dialogue"
