---
layout: post
title: "LaTeX for Jekyll from GitHub Pages to GitLab Pages"
excerpt_separator: "<!--more-->"
katex: true
tags:
  - Jekyll
  - blog
  - LaTeX
categories:
  - Computer Touching
---

About a year ago I [posted]({{ site.baseurl }}{% post_url 2021-05-19-latex-in-jekyll %})
about using <math display="inline">
    <mrow>
      <mtext>K</mtext>
      <mspace width="-0.17em" style="margin-left:-0.17em;"></mspace>
      <mpadded voffset="0.2em" height="+0.2em" depth="-0.2em">
        <mstyle scriptlevel="0" displaystyle="false">
          <mstyle scriptlevel="1" displaystyle="false">
            <mtext>A</mtext>
          </mstyle>
        </mstyle>
      </mpadded>
      <mspace width="-0.15em" style="margin-left:-0.15em;"></mspace>
      <mtext>T</mtext>
      <mspace width="-0.1667em" style="margin-left:-0.1667em;"></mspace>
      <mpadded voffset="-0.2155em" height="-0.2155em" depth="+0.2155em">
        <mstyle scriptlevel="0" displaystyle="false">
          <mtext>E</mtext>
        </mstyle>
      </mpadded>
      <mspace width="-0.125em" style="margin-left:-0.125em;"></mspace>
      <mtext>X</mtext>
    </mrow>
</math> in a Jekyll site for GitHub Pages.
This had been an unsatisfying solution to me, since GH Pages restricts Jekyll plugins to their
[allowlist](https://docs.github.com/en/pages/setting-up-a-github-pages-site-with-jekyll/about-github-pages-and-jekyll#plugins),
meaning that if I didn't want to manually add precompiled HTML pages,
LaTeX rendering would have to be done client-side using JavaScript.
Furthermore, I found out that kramdown, the Markdown compiler that GH Pages uses,
has [built-in support](https://kramdown.gettalong.org/math_engine/katex.html) for KaTeX
without needing to explicitly include additional Liquid tags, but GH Pages, too, has
[overridden configurations](https://docs.github.com/en/pages/setting-up-a-github-pages-site-with-jekyll/about-github-pages-and-jekyll#configuring-jekyll-in-your-github-pages-site)
needed to enable using KaTeX as well!

<!--more-->

On the other hand, GitLab Pages is much less restrictive with what's allowed,
because the build process is literally just a
[CI script](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html).
This bypasses the plugins allowlist, but not the configuration overrides, which are from the `github-pages` Gem.
The first thing to do, then, is to forego the Gem and include only the plugins I need in `Gemfile`.
On top of that, I have the Gems required to use KaTeX as kramdown's math engine.

```ruby
source "https://rubygems.org"

gem "jekyll-feed"
gem "jekyll-gist"
gem "jekyll-paginate"
gem "jekyll-remote-theme"
gem "jekyll-scholar"
gem "katex"
gem "kramdown-math-katex"
```

`github-pages` also specifies a number of [default configurations](https://github.com/github/pages-gem/blob/master/lib/github-pages/configuration.rb),
but most of these are either [Jekyll defaults](https://jekyllrb.com/docs/configuration/default/)
or undesirable (namely allowlist plugins and kramdown configurations).
I also already have a number of configurations set from when I was using GH Pages.
The below are the only missing configurations that needed adding in `_config.yml`
to include the Jekyll plugins and to enable using KaTeX for kramdown.
I use the [KaTeX option](https://katex.org/docs/options.html) to output MathML so that I don't need to include a stylesheet.

```yaml
plugins:
  - jekyll-feed
  - jekyll-gist
  - jekyll-paginate
  - jekyll-remote-theme
  - jekyll-scholar

kramdown:
  math_engine: katex
  math_engine_opts:
    output: mathml
```

Finally, to use GitLab Pages, there needs to be a CI configuration script to install Node.js
(which `kramdown-math-katex` needs to run KaTeX) and to build the Jekyll site.

```yaml
image: ruby:latest

pages:
  script:
    - apt-get update && apt-get -y install nodejs
    - gem install bundler
    - bundle install
    - bundle exec jekyll build -d public
  artifacts:
    paths:
      - public
```

Now inline LaTeX works everywhere using double dollar signs:
`$$\int_{\partial \Omega} \omega = \int_{\Omega} d\omega$$`
yields
$$\int_{\partial \Omega} \omega = \int_{\Omega} d\omega$$.
There aren't any delimiters for display-style math,
but you can always add `\displaystyle` to a block of LaTeX.

$$\displaystyle \int_{\partial \Omega} \omega = \int_{\Omega} d\omega$$

### References

* [About GitHub Pages and Jekyll](https://docs.github.com/en/pages/setting-up-a-github-pages-site-with-jekyll/about-github-pages-and-jekyll)
* [Tutorial: Create a GitLab Pages website from scratch](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html)
* [`kramdown-math-katex`](https://github.com/kramdown/math-katex)
* [Jekyll Default Configuration](https://jekyllrb.com/docs/configuration/default/)