---
layout: post
title: "Specialty Coffee Roasters in Philadelphia"
excerpt_separator: "<!--more-->"
tags:
  - Philadelphia
  - coffee
category:
  - Outside
---

Get your beans here!
This is a list of specialty coffee roasters local to Philly that do single-origin light roasts
whose coffeeshops are in or close to Centre City.
This list is tailored to my preferences:
I make coffee with an Aeropress at home,
and I enjoy getting pourover coffee when I'm out.

<!--more-->

These aren't in any particular order, and I'm reluctant to do any sort of ranking,
since I think they're all pretty great,
and I haven't had enough coffee from each of them to be able to assess them against one another.

#### [Habitat](https://habitat.coffee/)

**Location**: 11th and Spruce <br/>
**Hours**: until 18:00

They usually have a single-origin on batch brew until they run out, and they do run out.
Back in autumn 2023 they also had a spot at the Tuesday Rittenhouse farmers' markets
where they offered batch brew and pourovers (!),
but they don't seem to do that anymore.
I'm biased towards them because their farmers' market pourovers
was how I started getting into coffee :)

#### [Reanimator](https://www.reanimatorcoffee.com/)

**Locations**: 47th and Pine | Susquehanna and Norris (Fishtown) | others <br/>
**Hours**: until 15:00

This might be a seasonal thing, but I recently got a single-origin brew at the Fishtown location.
I haven't noticed if the West Philly location ever did,
but I have gotten a pourover there.
Interestingly, I've also seen their beans at Revolver in Vancouver!

#### [Ox](https://oxcoffee.com/)

**Location**: 3rd and South <br/>
**Hours**: until 13:00 (weekdays)/14:00 (weekends)

They have a single-origin on batch brew until they run out,
but I've always been able to get a cup when I've gone,
although they do close pretty early,
so you might miss them if you're an afternoon coffee person like me.
I don't think they offer pourovers though.
A few other coffee shops in Philly carry and brew their beans, such as K'Far.

#### [Vibrant](https://www.vibrantcoffeeroasters.com/)

**Location**: Rittenhouse Square and Locust <br/>
**Hours**: until 16:00

They only have batch-brewed single-origin on the weekends,
and they get pretty busy during the weekends!
Understandably, they don't do pourovers,
and their physical location is a pretty tiny place.

#### [Rival Bros](https://rivalbros.com/)

**Location**: 24th and Lombard | Passyunk and Tasker <br/>
**Hours**: until 18:00

They usually have around three selections for pourovers,
and the best part is the happy hour 14:00 – 16:00 where they're a dollar off.

#### [Peddler](https://www.peddlercoffee.com/)

**Location**: 21st and Spring (behind the Franklin Institute) <br/>
**Hours**: until 17:00

They pretty much have the same four roasts all year round,
and I believe typically offer two or three of them as pourovers.
(They also have a really good spicy chai latte made in-house.)

#### [Elixr](https://elixrcoffee.com/)

**Locations**: 37th and Market | b/w 15th & 16th and Walnut | others <br/>
**Hours**: varies by location

This is perhaps the most well-known specialty coffee roaster in Philly,
and many very nice coffeeshops will carry and brew their beans.

#### [Pilgrim](https://pilgrimroasters.com/)

**Location**: Main and Jamestown (Manayunk) <br/>
**Hours**: until 16:00

Relative to Centre City, they're pretty far away in Manayunk,
but it's a great specialty coffeeshop if you're in the area.
I had a single-origin brew during the Manayunk StrEAT Food Festival,
and I assume they also normally have it, along with pourovers.

## Other Roasters

#### [Ultimo](https://www.ultimocoffee.com/)

**Location**: 20th and Locust | 22nd and Catharine | 15th and Mifflin <br/>
**Hours**: until 18:00

#### [Greenstreet](https://www.greenstreetcoffee.com/)

**Location**: 15th and Federal (near Ellsworth-Federal station) <br/>
**Hours**: until 14:00

#### [Top Hat](https://tophat-espresso.com/)

**Location**: 32nd and Walnut <br/>
**Hours**: until 15:00 (weekdays)/16:00 (weekends)

#### [Nook](https://www.nookbakeryandcoffee.com/)

**Location**: 20th and Chestnut <br/>
**Hours**: until 15:30 (weekdays)/13:00 (Saturday)

Their roasting setup is physically in this shop,
and they offer an enormous selection of different beans and roasts.
They also do have the specialty coffee by-the-cup,
but I haven't seen how exactly it is they brew it,
because there's no pourover setup and it's an enormous 16 oz cup.
Their light roasts have always tasted kind of burnt to me...
I feel bad for saying it because the two people running the shop are really nice
and I really want to like their coffee!

#### [Persimmon](https://persimmoncoffee.com/)

**Location**: Frankford and Girard <br/>
**Hours**: until 15:00 (weekdays)/16:00 (weekends)

I believe they only ever have one roast of beans at a time,
so by default their batch brew would be that coffee.
A bit pricey, but the specialty drinks are really tasty.

#### [Cogito](https://www.cogitocoffee.com/)

**Location**: 12th and Sansom <br/>
**Hours**: permanently closed :(

This is apparently a Croatian coffee roaster with five locations in Croatia,
one in Dubai (??), and formerly one in Philadelphia (??).
I unfortunately never got to get a pourover from them before they closed...