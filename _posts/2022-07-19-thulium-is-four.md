---
layout: post
title: "Thulium is Four Years Old!"
tags:
  - server
  - Thulium
categories:
  - Computer Touching
---

Thulium, my personal server, turned four this year!
Its official birthday was a few months ago on 15 April.
A few more things have changed since I
[last posted]({{ site.baseurl }}{% post_url 2020-12-28-thulium-failure %}) about it,
so I thought I'd update with what's new.

<!--more-->

## VPSification

Even though I spent a good amount of time repairing the physical machine in which Thulium lived,
in anticipation of moving to an entirely different country for my PhD
(I wasn't going to physically lug a desktop tower with me everywhere),
I decided to move Thulium onto a VPS with Hetzner in October 2021.
I was also earning an income as a Master's student,
so I felt justified in spending money monthly on what barely constitutes a hobby
(I'm glad to be in a position where 7€/month is now peanuts to me).
I currently have the following with Hetzner:

* A CPX11 cloud server in Nuremberg, Germany with 2 vCPUs, 2 GB RAM, 40 GB disk space, and an IPv4 address for 3.99€/month;
* An additional attached 15 GB volume at 0.60€/month for my borg backups; and
* A BX10 storage box in Berlin, Germany with 100 GB storage at 2.90€/month for my photos backup.

I may turn the storage box into an attached volume later on.
It's a little more expensive at 0.04€/GB for a total of 4€/month,
but a little more flexible with the storage size,
and more convenient to access since the storage box doesn't really have SSH access.

So far, using a VPS has worked quite well with the added bonus that
Thulium doesn't go down every time my ISP decides to change my home IP address.
There is some lag when SSHing into the server,
probably because it's physically located in Germany,
but the internet connection is *so* much faster—`apt update` practically finishes instantaneously
compared to when it was in a box sitting in my home.

This brings us now, though, to a Ship of Theseus question, except it's the Server of Thulium:
is it still Thulium if I've reinstalled the entire server on my home machine in 2020
and then moved the entire server from my home machine to a VPS,
having changed out both the software and the hardware?

## ert.space is no longer

In October 2020, I decided to no longer renew my very first domain name, [ert.space](https://ert.space).
I already had my current domain, [ionathan.ch](https://ionathan.ch), since November 2019,
and I've now consolidated my server's web services and my website to both be accessed from ionathan.ch.
[hilb.ert.space](https://hilb.ert.space) is thus no more,
although in memory of it this blog is still named ⟨ortho|normal⟩.

<iframe src="https://cybre.space/@nonphatic/105097154125793062/embed"
  class="mastodon-embed"
  allowfullscreen="allowfullscreen">
</iframe>
<script src="https://cybre.space/embed.js" async="async"></script>

## Server services

In the [last retrospective]({{ site.baseurl }}{% post_url 2019-07-09-thulium-a-one-year-retrospective %})
from two (!) years ago, I listed out what I was running from my server.
Now, the cohort is:

* The same [Nextcloud](https://next.ionathan.ch) instance,
  although with a few [update mishaps](https://twitter.com/ionathanch/status/1491665236515909632) along the way;
* [Gitbert on Gitea](https://git.ionathan.ch) as always; and
* A [FreshRSS](https://rss.ionathan.ch) instance in place of TTRSS after the latter kept breaking across updates;

I'm no longer hosting my personal wiki, and is instead just a collection of files in a [GitHub wiki](https://github.com/ionathanch/ionathanch/wiki).
Some of these are org-mode files rather than Markdown files,
because I thought I'd finally learn how to use org mode in Emacs (I thought wrong).
Additionally, I've moved this website from GitHub Pages to GitLab Pages,
since it gives me more flexibility with the Ruby plugins,
which I've written about in a [previous post]({{ site.baseurl }}{% post_url 2022-05-22-jekyll-pages %}).

*Very* recently, I've also set up an MTA with Postfix on my server (not through Docker)
so that cron can send me emails when something goes wrong.
This was spurred by the fact that I ran out of disk space for borg to create new backups,
which failed silently, which resulted in a three-month gap in my backups.

<iframe src="https://types.pl/@ionchy/108613621509789075/embed"
  class="mastodon-embed"
  allowfullscreen="allowfullscreen">
</iframe>
<script src="https://types.pl/embed.js" async="async"></script>

It turns out it's a lot of work to get everything running smoothly!
My wiki page on [MTA with Postfix](https://github.com/ionathanch/ionathanch/wiki/MTA-with-Postfix)
has all the technical notes and annoying details.
It can receive mail as well, which I didn't originally set out to set up,
but until I actually see spam incoming I'll hold off on any more elaborate antispam measures like SpamAssassin.
I'm still debating whether to also set up a MUA and some sort of mail client to interact with Postfix.
Mail servers are so, so very complicated and convoluted.

<hr>

Overall, I'm quite pleased with how everything is now set up.
I don't usually need to go in to maintain anything,
except for updating Nextcloud when the GUI indicates there's a new version,
and now hopefully I'll immediately be notified by cron when there *is* something I need to handle.
Aside from considering moving the storage box to an attached volume and adding a MUA to my MTA,
I don't have any other major changes or additions planned.