---
layout: post
title: "Ice Cream Shops in Philadelphia"
excerpt_separator: "<!--more-->"
tags:
  - ice cream
  - Philadelphia
category:
  - Outside
---

When I arrived in Philadelphia, I started a
[Tweet thread of ice cream shops](https://twitter.com/ionathanch/status/1560720127410294792)
that are or aren't as good as [Rain or Shine](http://rainorshineicecream.com/) back in Vancouver,
which was the ice cream place I frequented most often since they have a location on the UBC campus.
(I don't have a thread or a list for ice cream shops in Vancouver,
but you really can't go wrong with Rain or Shine or with [Earnest](https://earnesticecream.com).)

Here I collect a list of the shops that *are* as good as Rain or Shine,
ordered from west to east.
Prices are for the smallest (non-kid's) scoop in a cup, including 8% tax, excluding tip.
<!--more-->
This isn't a total ranking of the ice cream shops,
because I think they're *all* worthwhile to visit if you want ice cream.
However, I've put stars 🌟 next to the ones I especially like,
so if you find yourself equidistant between an entry with a star and one without,
do go for the one with a star instead of flipping a coin.
I've also put pins 📍 next to the ones that are local to metro Philly,
as opposed to chains that can be found in other major US cities.

This list only has shops specializing in ice cream scoops
(which they call "hard" or "frozen" ice cream here, for some reason,
as if anyone would want unfrozen ice cream).
I will *not* include soft serve, rolled ice cream, shaved ice,
or large ice cream chains like Ben & Jerry's or Häagen-Dazs —
if I can buy a pint at a grocery store back in Vancouver,
there's no point including it on a Philly-specific list.
If you're curious about where I might visit next,
my [map of ice cream shops](https://goo.gl/maps/rPc43nxQxiQXZXqc7)
includes both places on this list and places I've yet to visit.
Photos can be found in the Twitter thread above,
or in my [Instagram guide](https://www.instagram.com/ionchyeats/guide/ice-cream-shops-in-philly/17866086527750207/).

<hr/>

### West Philly

#### [Weckerly's Ice Cream](https://www.weckerlys.com/) 📍 🌟
265 S 44th Street (bw Spruce & Locust) <br/>
Single scoop: 4.99$

### Brewerytown

#### [Harper's Ice Cream](https://www.harpersicecream.com/) 📍
2827 W Girard Avenue <br/>
Single scoop: 4.32$

#### [Over the Moon Ice Cream](https://www.overthemoonicecream.com/) 📍
2623 W Girard Avenue <br/>
Single scoop: 4$

### Centre City

#### [1-900-ICE-CREAM](https://1900icecream.com/) 📍 🌟
229 S. 20th Street (bw Walnut & Locust) <br/>
Single scoop: 6.48$

#### [Jeni's Splendid Ice Creams](https://jenis.com/)
1901 Chestnut Street <br/>
"Single" scoop (two flavours): 6.75$

#### [Van Leeuwen Ice Cream](https://vanleeuwenicecream.com/)
115 S 18th Street / 119 S 13th Street <br/>
Single scoop: 6.21$

#### [Vita](https://www.vitainphilly.com/) 🌟
261 S 17th Street <br/>
Small gelato (two flavours): 5.75$

#### [Bottega Rittenhouse](https://bottegarittenhouse.com/)
263 S 17th Street <br/>
Single scoop: 4.90$

#### [Gran Caffè L'Aquila](https://grancaffelaquila.com/)
1716 Chestnut Street <br/>
Small gelato: 5.94$

### Passyunk

#### [Milk Jawn](https://milkjawn.com/) 📍 🌟
9 E Passyunk Avenue (by Dickinson) <br/>
Single scoop: 5.94$

#### [Arctic Scoop](https://www.arcticscoopphilly.com/) 📍
2 E Passyunk Avenue (by 13th & Moore) <br/>
Single scoop (w/ topping and drizzle): 5.40$

### Bella Vista

#### Anthony's Italian Coffee & Chocolate House
903 S 9th Street (by Christian) <br/>
Single scoope: 5$

### Queen Village

#### [Cuzzy's Ice Cream Parlor](https://cuzzysicecream.com/) 📍 🌟
618 S 5th Street (by South) <br/>
Single scoop: 4.50$

### Fishtown

#### [Cloud Cups](https://thecloudcupscompany.com/) 📍
2311 Frankford Avenue (by Dauphin) <br/>
Single scoop: 5$

#### [Float Dreamery](https://floatdreamery.com/) 📍 🌟
1255 E Palmer Street (on Thompson) <br/>
Single scoop: 5$

### Mount Airy

#### [Zsa's Ice Cream](https://www.zsasicecream.com/) 📍 🌟
6616 Germantown Avenue <br/>
Single scoop: 5.94$

### Manayunk

#### [Tubby Robot Ice Cream Factory](https://www.tubbyrobot.com/) 📍
4369 Main Street <br/>
Single scoop: 5.12$

### Honourable Mentions

#### [Bodhi](https://www.bode-regional.com/)
Society Hill — 410 S 2nd Street <br/>
Oat milk ice cream: 5.40$ <br/>
<small>_A coffee shop that makes their own oat milk
and a very thick, pudding-like delicious oat ice cream._</small>

#### [Safa Plant Company](https://safaplantcompany.com/)
Manayunk — 4165 Main Street <br/>
Rose and saffron ice cream: 4.86$ <br/>
<small>_Not in the main list since it's not an ice cream shop,
but for some reason this teahouse-turned-plantstore has one (1) single ice cream flavour
and it's really good??_</small>

#### [Matcha Panda Cafe](https://www.matchapanda.com/)
Chinatown — 202 N 9th Street (by Race) <br/>
Soft serve: 7.29$ <br/>
<small>_They have a matcha and a hojicha flavour.
A little ridiculously expensive, but the flavour is excellent and very strong._</small>

#### [The Igloo](https://igloodesserts.com/)
Graduate Hospital — 2223 Grays Ferry Avenue (in front of Grays Ferry Triangle) <br/>
Single scoop: 5.93$ <br/>
<small>_Despite being froyo-focussed, they have surprisingly decent in-house ice cream as well._</small>

#### [Vanderwende Farm Creamery](https://www.vanderwendefarmcreamery.com/)
Centre City — 243 Market Street <br/>
Single scoop: 6.48$ <br/>
<small>_According to their site, they use milk from their own cows, which I think is very unique and impressive. <br/>
A shame that the ice cream is incredibly melty, way too sugary, and overall not that good._</small>