---
layout: post
title: "Little Shops in Philadelphia"
excerpt_separator: "<!--more-->"
tags:
  - Philadelphia
category:
  - Outside
---

I've previously posted about my favourite
[ice cream shops]({{ site.baseurl }}{% post_url 2022-09-18-philly-ice-cream %})
in Philadelphia, but there's a ton of other great little shops to visit.
Here's a few that I've really liked,
sorted into two categories: used bookstores and vintage/oddities.
I've excluded all food places, including
coffeeshops, roasteries, bakeries, dessert shops, grocers, markets, food trucks, and so on,
because there are just too many of them.

<!--more-->

<hr/>

<figure style="font-size: small; float: right; margin-top: 0; margin-bottom: 2rem;">
<blockquote style="padding-top: 0; padding-bottom: 0;">
<i>I love [a] little shop.</i>
</blockquote>
<figcaption>
— The Doctor, <i>New Earth</i>, Doctor Who (2005) series 2
</figcaption>
</figure>

## Used Bookstores

#### [Giovanni's Room](http://www.queerbooks.com/)
_Washington Square — 345 S 12th Street_ <br/>
One of Philly's largest and oldest queer bookstores!
They carry both used books and a lot of new queer fiction,
as well as some secondhand clothing and other miscellany.

#### [Wooden Shoe Books](http://www.woodenshoebooks.org/)
_South Street — 704 South Street_ <br/>
A small volunteer-run leftist anarchist bookstore.
They have a lot of leftist materials you wouldn't see elsewhere and a lot of zines.

#### [Mostly Books](http://mostlybooksphilly.com/)
_Queen Village — 529 Bainbridge Street_ <br/>
With an unassuming storefront,
they have a HUGE two-storey warehouse of books in the back.
This is probably the largest used bookstore in Philly.

#### [Molly's Books and Records](http://www.mollysbooksandrecords.com/)
_Bella Vista — 1010 S 9th Street_ <br/>
What can I say?
It's a small bookstore with records (or a record store with books) and good vibes.

#### [Neighborhood Books](https://bookshop.org/shop/neighborhoodbooks215)
_Graduate Hospital — 1906 South Street_ <br/>
A cosy bookstore with lots of well-organized fiction upstairs and graphic novels downstairs.

#### [Book Corner](https://bookshop.org/shop/BookCornerphilly)
_Logan Square — 311 N 20th Street_ <br/>
A used bookstore I think associated with the public libraries of Philly.

#### The Last Word Bookshop
_University City — 220 S 40th Street_ <br/>
The nearest used bookstore to campus, which is super convenient for me.
Sometimes there's a bookstore cat hanging around!
Open surprisingly late.

#### [House of Our Own Books](https://www.biblio.com/bookstore/house-of-our-own-books-philadelphia)
_University City — 3920 Spruce Street_ <br/>
A Victorian rowhouse turned used bookstore with teetering towers of books.
The fiction selection upstairs is very large and tends to be older and more literary.

#### [The Book Trader](http://www.phillybooktrader.com/)
_Old City — 7 N 2nd Street_ <br/>
A true labyrinth of a bookstore, with a similarly large and old collection of fiction upstairs.
They also have a lovely bookstore cat!

#### [Bindlestiff Books](http://bindlestiffbooks.wordpress.com/)
_Spruce Hill — 4530 Baltimore Avenue_ <br/>
Just a quaint little bookstore, what more could you ask for?

<br/>

## Vintage, Thrift, and Oddities

#### [South Street Art Mart](https://southstreetartmart.com/)
_South Street – 530 S 4th Street_ <br/>
A very cool shop full of local art!
They've got zines, stickers, pins, keychains, cards, prints, clothes, and so on.

#### [Little Devils Curiosities](https://www.instagram.com/littledevilscuriosities/)
_Fishtown – 2158 E Dauphin Street_ <br/>
<del>Imagine goth meets vintage.
You'll find jewellery and trinkets of that aesthetic, spiders and skulls and sigils,
and some amazing taxidermy pieces too.</del> <br/>
This shop is now [closed](https://www.instagram.com/p/C2AdaWFr9VH) 😔

#### [Thunderbird Salvage](https://www.instagram.com/thunderbirdsalvage)
_East Kensington — 2441 Frankford Avenue_ <br/>
A large vintage thrift store packed with everything from clothes to books to furniture.
A real treasurehunt of a shop.

#### [Raxx Vintage Emporium](https://www.depop.com/raxxvintage/)
_South Street – 534 South Street_ <br/>
High quality, curated vintage clothing and other oddities.
Note that they very much emphasize _not_ being a thrift store!

#### [A Four Foot Prune](https://www.afourfootprune.com/)
_Old City – 142 North 2nd Street_ <br/>
Less clothing but a wider range of cute vintageware,
including toys and household goods.

#### Jinxed
_Fishtown; Spruce Hill; Graduate Hospital_ <br/>
Similar to A Four Foot Prune,
there's three locations of antique furniture, houseware, and trinkets.

#### [Philly AIDS Thrift](https://www.phillyaidsthrift.com/)
_South Street – 710 S 5th Street_ <br/>
Sister store to Giovanni's Room,
with a larger selection of clothing and houseware,
and a decent amount of books as well.

#### [Buffalo Exchange](https://buffaloexchange.com/)
_Centre City – 1520 Chestnut Street_ <br/>
A more traditional thrift clothing store with racks and racks and racks of clothes.

#### [Safa Plant Co.](https://safaplantcompany.com/)
_Manayunk – 4165 Main Street_ <br/>
A combination plant store and teashop.
Yes I know I said I wouldn't include any food places but this place is really cute and I couldn't not.