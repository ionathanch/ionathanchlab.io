---
layout: post
title: "It's not all it's cracked up to be"
excerpt_separator: "<!--more-->"
tags:
  - laptop
  - screen
categories:
  - Computer Touching
---

Friday, 22 October 2021, 10:30 am. Cold, rainy, wet.
From inside Joyce–Collingwood station I spotted the R4, just as it was closing its back doors.
My first mistake was hope: The front door was not yet closed.
My second mistake was hubris: I _will_ make it to the bus if I run.
And so I ran, but my normally-grippy sneakers slipped on the sidewalk slick with rainwater,
and I ended up landing _hard_ on my ass right in front of the bus driver.

It was a miracle I ended up mostly physically unscathed.
I instinctively shot out my arms underneath me and I could have broken a wrist or two.
I could've landed on my tailbone wrong and broken that too.
But it seemed the only damage was a light bruise on my sacrum (had to look up the word for that) and to my ego.
Not even a scratch on my palms!
Then I sat down, took out my laptop, opened it, and discovered the large crack from the bottom left corner,
reaching longingly to conquer the entirety of the screen.

<!--more-->

{% include image.html
           img="/assets/images/laptop-screen/crack.jpg"
           width="50%"
           title="The left half of my laptop screen with a diagonal crack from the lower left corner,
              where the area of the screen above the crack is entirely black,
              while the rest of my desktop is visible,
              including the tail of a BLÅHAJ on a clothes drying rack."
           caption="Yes, my desktop background is BLÅHAJ soft toy shark, 100 cm." %}

## Step 0: Wallow in despair

This step is really important.
If you don't wallow in the despair now, when you're powerless on the bus,
the despair will overtake you when you actually want to take action.
Allow it to pass over and through you.[^1]
Now we are ready to fix the screen.

## Step 1: Ask a friend

I remembered a [friend](https://fbanados.github.io/) in my research lab had also replaced their ThinkPad screen,
who got their replacement from [Laptop Screen](https://www.laptopscreen.com/).
Searching for my model, I found the appropriate replacement;
unfortunately I couldn't upgrade from FHD to QHD, because the former uses a 30-pin connected while the latter 40-pin,
but I could upgrade to an IPS screen with "professional colour", if that even makes a noticeable difference.

{% include image.html
           img="/assets/images/laptop-screen/laptop-screen.png"
           title="Compatibility: Lenovo THINKPAD T480S SERIES
            Part Type: LCD Screen
            Size: 14.0″ WideScreen
            Resolution: FHD (1920x1080)
            Surface Type: Matte
            Video Connector: 30 pin video connector
            Mountings: Top and Bottom Brackets
            Optical Technology: IPS
            Condition: New Grade A+
            Warranty: Lifetime
            Note: Make sure new screen has same size, resolution, connector type as your old one!
            You can upgrade your screen: Upgrade Options
            $126.99 (striked out)
            $114.29"
           caption="They had the full 126.99$ price when I ordered it 😡" %}

{% include image.html
           img="/assets/images/laptop-screen/laptop-screen-upgrade.png"
           title="Upgrade Options: +$6.35  Matte IPS, 1080p, Wide viewing angles and professional color range
            Extended description of upgrade features (it does not mean that the upgrade(s) option(s) shown above include all of these features):
            IPS (In-plane switching) - Better viewing angles because of a different way of constructing the pixels. Colors will no longer shift if you look at your display from the side or from above.
            IPS Professional Color - A wider range of physical colors reproducible by the LCD. A must-have for professional photography, print and publishing, available to you at a fraction of the cost.
            120Hz - A maximum refresh rate of the display (most commonly 60Hz) dictates how many pictures your display can show per second. With a 120 Hertz upgrade, your new display can deliver double the frames each second for smoother motion and a much more immersive visual experience.
            120Hz IPS - All of the advantages of a 120Hertz upgrade combined with wide viewing angles and improved color range."
           caption="What do you think an unprofessional colour range is like?" %}

Even though I could've transited to their warehouse in under an hour and a half,
there wasn't a pickup option, so I had to pay 9.86$ for shipping and 2.53$ for insurance.
Then with GST and PST, the total came to **163.22$**.
High price for a slipping accident.

## Step 3: This is the boring part

I ordered it on Friday and they managed to get it shipped the same day,
but since Canada Post doesn't deliver on weekends,
I had to wait out the weekend.
I spent this time setting up my brother's monitor temporarily and making the crack worse because I couldn't resist.

<blockquote class="twitter-tweet">
<p lang="en" dir="ltr">
In other news the laptop screen is not doing great.
idk if you can see the flickering but this has gone from
"I can use ¾ of the screen I guess" to "no ❤️"
<a href="https://pic.twitter.com/1XBET9qeem">pic.twitter.com/1XBET9qeem</a>
</p>
&mdash; 🥢 {#- OPTIONS --sized-types -#} 🎃 (@ionathanch)
<a href="https://twitter.com/ionathanch/status/1451973099696443394">October 23, 2021</a>
</blockquote>
<script async src="/assets/js/twitter-widgets.js" charset="utf-8"></script> 

## Step 4: It's here!

Now it's time for the fun part: replacing the screen.
Before I started, I watched through [this](https://youtu.be/BpWZnoKFVNU) tutorial,
which was pretty close to how my model (the ThinkPad T480s) works.[^2]
I first peeled off the soft plastic bezel that was attached on with long, black adhesive strips.
I didn't have a spudger, so I did accidentally scratch one part of the outside of the bezel with my flathead.
Some of the adhesive strips were left stuck to the screen, so I had to peel them off and stick them back on the bezel.

{% include image.html
           img="/assets/images/laptop-screen/bezel.jpg"
           title="The back side of the rectangular plastic bezel."
           caption="This bezel came out as straight as I did." %}

Next part is easy: Remove the four screws holding the screen in place and unplug the 30-pin connector.
There's this thin metal latch that folds down to secure the plug; it has to be flipped up first.
I didn't notice it at first, because I wasn't expecting it from what I saw in the tutorial,
and I was nearly about to rip the cable in half.

{% include image.html
           img="/assets/images/laptop-screen/latch.jpg"
           title="A 30-pin connector plugged in, with a thin metal latch lifted up."
           caption="If you zoom in and count you can see there really are 30 pins." %}

Now I can plug in the new screen, flip the latch down, turn on my computer briefly to test the screen,
screw the new screen in firmly, peel off the protective plastic, and finally put the bezel back.
This was probably the most difficult step and took me three tries to get it aligned correctly.
And it's done! The screen looks brand new (because it is), and you can barely tell where I scratched the bezel.

{% include image.html
           img="/assets/images/laptop-screen/booting.jpg"
           title="Laptop boot screen."
           caption="It's alive!! It's alive!!" %}

## Step 5: Shop for a laptop sleeve

Preferrably one with very padded sides to prevent similar drop damage.
I don't want to have to spend another 163.22$ for my mistakes.

<!--
<iframe src="https://types.pl/@ionchy/107147029510592712/embed"
  class="mastodon-embed"
  allowfullscreen="allowfullscreen">
</iframe>
<script src="/assets/js/mastodon-embed.js" async="async"></script>
-->

<blockquote class="mastodon-embed" data-embed-url="https://types.pl/@ionchy/107147029510592712/embed" style="background: #FCF8FF; border-radius: 8px; border: 1px solid #C9C4DA; margin: 0; max-width: 540px; min-width: 270px; overflow: hidden; padding: 0;"> <a href="https://types.pl/@ionchy/107147029510592712" target="_blank" style="align-items: center; color: #1C1A25; display: flex; flex-direction: column; font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans', 'Helvetica Neue', Roboto, sans-serif; font-size: 14px; justify-content: center; letter-spacing: 0.25px; line-height: 20px; padding: 24px; text-decoration: none;"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="32" height="32" viewBox="0 0 79 75"><path d="M74.7135 16.6043C73.6199 8.54587 66.5351 2.19527 58.1366 0.964691C56.7196 0.756754 51.351 0 38.9148 0H38.822C26.3824 0 23.7135 0.756754 22.2966 0.964691C14.1319 2.16118 6.67571 7.86752 4.86669 16.0214C3.99657 20.0369 3.90371 24.4888 4.06535 28.5726C4.29578 34.4289 4.34049 40.275 4.877 46.1075C5.24791 49.9817 5.89495 53.8251 6.81328 57.6088C8.53288 64.5968 15.4938 70.4122 22.3138 72.7848C29.6155 75.259 37.468 75.6697 44.9919 73.971C45.8196 73.7801 46.6381 73.5586 47.4475 73.3063C49.2737 72.7302 51.4164 72.086 52.9915 70.9542C53.0131 70.9384 53.0308 70.9178 53.0433 70.8942C53.0558 70.8706 53.0628 70.8445 53.0637 70.8179V65.1661C53.0634 65.1412 53.0574 65.1167 53.0462 65.0944C53.035 65.0721 53.0189 65.0525 52.9992 65.0371C52.9794 65.0218 52.9564 65.011 52.9318 65.0056C52.9073 65.0002 52.8819 65.0003 52.8574 65.0059C48.0369 66.1472 43.0971 66.7193 38.141 66.7103C29.6118 66.7103 27.3178 62.6981 26.6609 61.0278C26.1329 59.5842 25.7976 58.0784 25.6636 56.5486C25.6622 56.5229 25.667 56.4973 25.6775 56.4738C25.688 56.4502 25.7039 56.4295 25.724 56.4132C25.7441 56.397 25.7678 56.3856 25.7931 56.3801C25.8185 56.3746 25.8448 56.3751 25.8699 56.3816C30.6101 57.5151 35.4693 58.0873 40.3455 58.086C41.5183 58.086 42.6876 58.086 43.8604 58.0553C48.7647 57.919 53.9339 57.6701 58.7591 56.7361C58.8794 56.7123 58.9998 56.6918 59.103 56.6611C66.7139 55.2124 73.9569 50.665 74.6929 39.1501C74.7204 38.6967 74.7892 34.4016 74.7892 33.9312C74.7926 32.3325 75.3085 22.5901 74.7135 16.6043ZM62.9996 45.3371H54.9966V25.9069C54.9966 21.8163 53.277 19.7302 49.7793 19.7302C45.9343 19.7302 44.0083 22.1981 44.0083 27.0727V37.7082H36.0534V27.0727C36.0534 22.1981 34.124 19.7302 30.279 19.7302C26.8019 19.7302 25.0651 21.8163 25.0617 25.9069V45.3371H17.0656V25.3172C17.0656 21.2266 18.1191 17.9769 20.2262 15.568C22.3998 13.1648 25.2509 11.9308 28.7898 11.9308C32.8859 11.9308 35.9812 13.492 38.0447 16.6111L40.036 19.9245L42.0308 16.6111C44.0943 13.492 47.1896 11.9308 51.2788 11.9308C54.8143 11.9308 57.6654 13.1648 59.8459 15.568C61.9529 17.9746 63.0065 21.2243 63.0065 25.3172L62.9996 45.3371Z" fill="currentColor"/></svg> <div style="color: #787588; margin-top: 16px;">Post by @ionchy@types.pl</div> <div style="font-weight: 500;">View on Mastodon</div> </a> </blockquote> <script data-allowed-prefixes="https://types.pl/" async src="https://types.pl/embed.js"></script>

---

[^1]: Excuse the reference, Dune (2021) with Paul played by renowned twink Timothée Chalamet just came out a few days ago.
[^2]: This [Reddit post](https://old.reddit.com/r/thinkpad/comments/9wfq9p/thinkpad_t480s_fhd_to_qhd_upgrade_log_applies_to/)
      is also a good textual reference.
