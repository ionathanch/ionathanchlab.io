---
layout: post
title: "Respooled Film Stocks"
excerpt_separator: "<!--more-->"
tags:
  - film photography
category:
  - Miscellany
---

<small><i>This was originally [posted on cohost](https://cohost.org/ionchy/post/4367819-35mm-colour-film).</i></small>

While looking up the various colour films my local photo shops carry,
I found that a lot of them are actually respools and repackagings of other film (most Kodak),
so I've tried to compile that information here to keep track of them all.

<!--more-->

Kodak's cinema film, which comes gigantic spools,
is a popular choice for respooling into canisters.
The C41 respools have the remjet backing removed so they produce some amount of halation,
while the ECN2 respools have not but most photo labs (at least the ones near me) don't handle it.

| Cinema film | C41 respools | ECN2 respools |
| - | - | - |
| Vision3 500T (5219) | CineStill 800T, RETO Amber T800, Reflx Lab 800 tungsten, Reformed Night 800, Candido 800, FilmNeverDie UMI 800 | Flic Film Cine Colour 500T, Reflx Lab 500T, Silbersaltz35 500T, FilmNeverDie SUIBO 500T |
| Vision3 200T (5213)| RETO Amber T200, Reflx Lab 200 tungsten, Candido 200 | Flic Film Cine Colour 200T, Reflx Lab 200T, Silbersaltz35 200T, Hitchcock 200T 5213 |
| Vision3 250D (5207) | CineStill 400D, RETO Amber D400, Reflx Lab 400 daylight, Reformed Day 400, Candido 400, FilmNeverDie SORA 200 | Flic Film Cine Colour 250D, Reflx Lab 250D, Silbersaltz35 250D, FilmNeverDie KUMO 250D |
| Vision3 50D (5203) | CineStill 50D, RETO Amber D100, Reflx Lab 100 daylight | Flic Film Cine Colour 50D, Reflx Lab 50D, Silbersaltz35 50D, Hitchcock 50D 5203 |
| Vision3 Color Digital Intermediate (2254[^1]) | FPP Low ISO Color | N/A (no remjet) |

| Consumer C41 film | Repackagings |
| - | - |
| UltraMax 800 | Lomography Color 800 |
| UltraMax 400 | Fujifilm 400, Lomography Color 400 |
| Gold 200 | Fujifilm 200, Lomography Color 200 |
| ColorPlus 200 | |
| ProImage 100 | Lomography Color 100 (?) |

<!--
| Professional C41 film | Repackagings |
| - | - |
| Portra 800 | |
| Portra 400 | |
| Portra 160 | |
| Ektar 100 | |
-->

| Other | Process | Respools/repackagings |
| - | - | - |
| Aerocolor IV 2460 | C41 | Flic Film Elektra 100, Reflx Lab Pro 100, CatLABS X Film 100, SantaColor 100, Popho Luminar 100, Film Washi X, Karmir 160, FPP Color 125/Astrum 125 (?) |
| Ektachrome 100D | E6 | Flic Film Chrome 100, Reflx Lab 100R, FPP Chrome, FilmNeverDie CHAMELEON 100 |

Film Film has other respools of Kodak film, but it's unclear exactly what they are respools of:

* Aurora 800 — Could be Portra 800 or UltraMax 800;
  see [Brooklyn Film Camera's comparison](https://www.youtube.com/watch?v=8-nRupBAiik)
* Street Candy Street Savvy 400 — Could be UltraMax 400 or Portra 400;
  see [Reformed Film Lab's comparison](https://www.youtube.com/watch?v=7ixF4ynZ5OI) with the former

Finally, here are the films that I've found, all C41 process,
that appear to be genuinely independent of Kodak:

* ORWO Wolfen NC500, NC400
  * [Ilford Ilfocolor 400](https://www.analog.cafe/r/harman-phoenix-200-vs-ilford-ilfocolor-400-md99),
    [AgfaPhoto Color 400](https://forum.aphog.com/index.php?thread/62588-agfaphoto-color-400/),
    [Flic Film Street Candy Psychedlic 400](https://www.youtube.com/watch?v=wHOoIDf1eHw),
    and [Shenguang 400](https://www.reddit.com/r/AnalogCommunity/comments/1cfxivg/shenguangshanghai_color_400is_it_just_wolfen_ncs/)
    are speculated to be repackagings
* Harman Phoenix 200
* FujiColor 100 and Superia Premium 400 (Japan only)

And some further mysteries...

* Silberra Color 50/100/160 — Speculated to be another Aerocolor IV respool,
  but it's unclear how it comes in three different speeds
* KONO! Color 400 — I haven't been able to find any additional information other than what their website says.
  I can't even find anyone who's reviewed it yet,
  despite having been out since October 2023
* FPP SUN Color — An ISO 1 film described as "a technical film designed for making contact prints in a lab",
  so it's not a Kodak cinema film respool,
  and I have no idea what kind of special film gets used for negative contact prints specifically

I'm personally not looking for "experimental" colour films or films that aren't "true colour",
so I've excluded a number of those above, which are mostly:

* LomoChrome films from Lomography
* RETOCOLOR films from RETO Project
* various from KONO!
* various from Film Photography Project
* Zombie 400 from
  [Mr. Negative](https://mrnegative.com.au/products/zombie-400-filmneverdie-x-mr-negative-collaboration-35mm-36exp) ×
  [FilmNeverDie](https://filmneverdie.com/collections/35mm-film/products/zombie-400-filmneverdie-x-mr-negative-collaboration-35mm-36exp)

---

[^1]: Kodak makes a few other colour intermediate cinema films.
      According to the spec sheet for VISION Color Intermediate,
      2242 is a 35 mm film, 3242 is a 16 mm film,
      and 5242 is a 65 mm film with a remjet backing.
      FPP specifically mentions that their film was intended for "digital dupes",
      so it's likely it's the colour digital intermediate and not 2242.