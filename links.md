---
layout: page
title: Links
sidebar_link: true
---

<style>
  hr {
    margin: 1rem 0;
  }

  input {
    display: none;
  }

  nav {
    margin-top: 0.5rem;
    text-align: left;
  }

  label {
    cursor: pointer;
    padding-top: 0.3rem;
    margin-right: 1.5rem;
    text-wrap: nowrap;
    opacity: 0.75;
    h4 {
      display: inline;
      font-weight: normal;
    }
    &:hover {
      opacity: 1;
    }
  }

  section {
    display: none;
  }

  #input1:checked ~ #tab1,
  #input2:checked ~ #tab2,
  #input3:checked ~ #tab3,
  #input4:checked ~ #tab4,
  #input5:checked ~ #tab5,
  #input6:checked ~ #tab6 {
    display: block;
  }

  #input1:checked ~ nav > #label1,
  #input2:checked ~ nav > #label2,
  #input3:checked ~ nav > #label3,
  #input4:checked ~ nav > #label4,
  #input5:checked ~ nav > #label5,
  #input6:checked ~ nav > #label6 {
    opacity: 1;
    border-top: dotted rgb(118, 118, 118, 0.9);
  }
</style>

<div>
<input type="radio" id="input1" name="links" checked>
<input type="radio" id="input2" name="links">
<input type="radio" id="input3" name="links">
<input type="radio" id="input4" name="links">
<input type="radio" id="input5" name="links">
<input type="radio" id="input6" name="links">

<nav id="links">
  <label id="label1" for="input1"><h4>personal contact</h4></label>
  <label id="label2" for="input2"><h4>academic tools</h4></label>
  <label id="label3" for="input3"><h4>other tools</h4></label>
  <label id="label4" for="input4"><h4>knowledge bases</h4></label>
  <label id="label5" for="input5"><h4>PL reference</h4></label>
  <label id="label6" for="input6"><h4>misc. articles</h4></label>
</nav>

<hr/>

<section id="tab1" class="tab-content" markdown="1">

Aside from the contact information in my CV, here are where you can find me online.

#### academic

| **Scholar**      | [dR8doS8AAAAJ](https://scholar.google.com/citations?user=dR8doS8AAAAJ) |
| **ResearchGate** | [Jonathan-Chan-47](https://www.researchgate.net/profile/Jonathan-Chan-47) |
| **ACM**          | [99661092766](https://dl.acm.org/profile/99661092766) |
| **GitHub**       | [@ionathanch](https://github.com/ionathanch) |
| **Codeberg**     | [@ionchy](https://codeberg.org/ionchy) |
| **Gitbert**      | [@ionchy](https://git.ionchy.ca/ionchy) |

#### social

| **types.pl**   | [@ionchy](https://types.pl/@ionchy) |
| **Bluesky**    | [@ionchy.ca](https://bsky.app/profile/ionchy.ca) |
| **Twitter**    | [@ionathanch](https://twitter.com/ionathanch) |
| **Instagram**  | [@ionchyeats](https://instagram.com/ionchyeats) |
| **last.fm**    | [ionchy](https://www.last.fm/user/ionchy) |
| **StoryGraph** | [ionchy](https://app.thestorygraph.com/profile/ionchy) |
| **Letterboxd** | [ionchy](https://letterboxd.com/ionchy) |
| **Beli**       | [ionchy](https://beliapp.co/app/ionchy) |
| **Flickr**     | [ionchy](https://flickr.com/photos/ionchy) |

</section>
<section id="tab2" class="tab-content" markdown="1">

#### LaTeX

* [BibTeX Tidy](https://flamingtempura.github.io/bibtex-tidy/index.html) —
  prettifier for BibTeX entries
* [The Comprehensive LaTeX Symbol List](https://tug.ctan.org/info/symbols/comprehensive/symbols-a4.pdf) —
  30 MB PDF containing an index of symbols from all LaTeX packages
* [Detexify](https://detexify.kirelabs.org) —
  handwritten symbol recognizer & symbol macro search
* [LaTeX Font Catalogue](https://tug.org/FontCatalogue/) —
  fonts available with most LaTeX distributions
* [tikzcd-editor](https://tikzcd.yichuanshen.de/) —
  simple UI for creating commutative diagrams in TikZ
* [quiver](https://q.uiver.app/) —
  fancier UI for creating commutative diagrams in TikZ
* [Temml](https://temml.org/) —
  convert TeX to MathML
* [pdfsizeopt](https://github.com/pts/pdfsizeopt) —
  shrink size of PDFs

#### research

* [sci-hub.se](https://sci-hub.se/), [sci-hub.st](https://sci-hub.st/), [sci-hub.ru](https://sci-hub.ru/) —
  paywalled published articles by DOI
* [archive.today](https://archive.is/) —
  paywalled news articles by original URL
* [12ft.io](https://12ft.io/) — paywalled news articles by original URL
* [libgen.rs](https://libgen.rs/), [libgen.is](https://libgen.is/), [libgen.st](https://libgen.st/) —
  mostly textbooks and ebooks
* [z-lib](https://z-library.sk/) — more textbooks and ebooks
* [Anna's Archive](https://annas-archive.org/) —
  comprehensive archive encompassing much of the above
* Web of Science ([UBC](https://resources.library.ubc.ca/page.php?id=138), [UPenn](http://hdl.library.upenn.edu/1017/6959)) —
  institution-paid (if you're lucky) article index

<div></div>
</section>
<section id="tab3" class="tab-content" markdown="1">

#### media

* [cobalt](https://cobalt.tools/) —
  download media anywhere
* [Songlink/Odesli](https://odesli.co/) —
  link to a song on all streaming services
* [remove.bg](https://www.remove.bg/) —
  remove background from images
* [Dither Me This](https://doodad.dev/dither-me-this/) —
  various dithering methods
* [gifiddle](https://ata4.github.io/gifiddle/) —
  correct-to-spec GIF viewer
* [Subtitle Tools](https://subtitletools.com/) —
  sync, convert, and combine subtitle files
* [MapartCraft](https://rebane2001.com/mapartcraft/) —
  Minecraft mapart generator

#### \#posting

* [Shapecatcher](http://shapecatcher.com/) —
  handwritten symbol recognizer for Unicode characters
* [Unicode Toys](https://qaz.wtf/u/) —
  find and transform Unicode text
* [You wouldn't steal a meme](https://youwouldntsteala.website/editor.html) —
  generator for "You wouldn't steal a car" template
* [Carbon](https://carbon.now.sh/) —
  generate syntax highlighted code as images
* [markdown PLUS](https://oat.zone/markdown-plus/) —
  extended markup for cohost (may it rest in peace)
* [prechoster](https://cloudwithlightning.net/random/chostin/prechoster/) —
  preprocessor for HTML + CSS in cohost (may it rest in peace)
* [pointfree.io](https://pointfree.io) —
  convert Haskell into pointfree Haskell

<div></div>
</section>
<section id="tab4" class="tab-content" markdown="1">

#### wikis

* [PL Conferences](https://pl-conferences.com/) —
  dates and deadlines for upcoming PL conferences
* [1lab](https://1lab.dev/) —
  univalent mathematics in Cubical Agda
* [PLS lab](https://www.pls-lab.org/) —
  programming, logic, and semantics encyclopedia
* [Camera-wiki](http://camera-wiki.org/wiki/Main_Page) —
  reference guide for all cameras
* [IMSLP](https://imslp.org/wiki/Main_Page) —
  public domain sheet music
* [TripSit](https://wiki.tripsit.me/wiki/Main_Page) —
  guides and information for harm reduction
* [Megathread](https://rentry.co/megathread) —
  anything and everything

#### webpages

* [syntax across languages](https://rigaux.org/language-study/syntax-across-languages/) —
  comparison of syntaxes categorized by feature
* [Cantor's attic](https://neugierde.github.io/cantors-attic/) —
  infinite ordinals and cardinals
* [Erich's Packing Center](https://erich-friedman.github.io/packing/) —
  packing 2D shapes within other shapes
* [sphere distribution problem](https://bendwavy.org/sphere.htm) —
  arranging points evenly on a sphere
* [TLCA List of Open Problems](https://tlca.di.unito.it/opltlca/) —
  open problems on typed lambda calculi
* [Logic Matters](https://www.logicmatters.net/) –
  guides to resources for (and, recently, textbooks on) logic and category theory
* [Is My MP a Landlord?](https://ismympalandlord.ca/) —
  database of Canadian MPs and their investments
* [CameraQuest](https://www.cameraquest.com/classics.htm) —
  profiles of cameras, mostly rangefinders and SLRs
* [125px Tech Documents](https://125px.com/techdocs/) —
  technical documents from various photographic film manufacturers
* [CleanAirCrew](https://cleanaircrew.org/) —
  resources for air filtration, ventilation, and masking
* [Optimize your Biology](https://optimizeyourbiology.com/) —
  product reviews for lighting products
* [PinLord](https://www.pinlordshop.com/) —
  guides for making custom enamel pins and other merch

<div></div>
</section>
<section id="tab5" class="tab-content" markdown="1">

#### Rocq

* [Rocq manual](https://rocq-prover.org/doc/master/refman/index.html)
  * [SSReflect manual](https://rocq-prover.org/doc/master/refman/proof-engine/ssreflect-proof-language.html)
* [Ott manual](https://www.cl.cam.ac.uk/~pes20/ott/top2.html)
* [Coqhammer](https://coqhammer.github.io/)

#### Agda

* [Agda manual](https://agda.readthedocs.io/en/latest/index.html)
  * [emacs mode](https://agda.readthedocs.io/en/latest/tools/emacs-mode.html)
* [agda-stdlib index](https://agda.github.io/agda-stdlib/master/)

#### Lean

* [Lean documentation](https://lean-lang.org/lean4/doc/)
* [Lean reference](https://lean-lang.org/doc/reference/latest/)
* [mathlib4 documentation](https://leanprover-community.github.io/mathlib4_docs/)
* [Lean 4 Web](https://live.lean-lang.org/)

#### Haskell

* [Hoogle](https://hoogle.haskell.org/)
* [GHC user's guide](https://ghc.gitlab.haskell.org/ghc/doc/users_guide/index.html)
* [Haskell 2010 report](https://www.haskell.org/onlinereport/index.html)
  * [Precedences and fixities of prelude operators](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-820004.4.2)
* [Haskell 98 report](https://www.haskell.org/onlinereport/index.html)

#### Conor McBride

* [Epilogue](https://mazzo.li/epilogue/index.html) (previously [here](http://www.e-pig.org/epilogue/index.html)) —
  posts by the Epigram team from 2005 – 2011
* [pigworker in a space](https://pigworker.wordpress.com/) — blog posts
* websites at [Strathclyde](https://personal.cis.strath.ac.uk/conor.mcbride/) and
  [strictlypositive.org](http://strictlypositive.org/)
  * [Let's see how things unfold: reconciling the infinite with the intensional](http://strictlypositive.org/ObsCoin.pdf)
  * [Grins from my Ripley Cupboard](http://strictlypositive.org/Ripley.pdf)
  * [The Derivative of a Regular Type is its Type of One-Hole Contexts](http://strictlypositive.org/diff.pdf)
  * [Perhaps Not The Answer You Were Expecting But You Asked For It (An Accidental Blook)](https://personal.cis.strath.ac.uk/conor.mcbride/so-pigworker.pdf)

#### other

* [Why must inductives be strictly positive?](https://vilhelms.github.io/posts/why-must-inductive-types-be-strictly-positive/) —
  Vilhelm Sjöberg, 9 April 2015
* [Inductive–Recursive Descriptions](https://spire-lang.org/blog/2014/08/04/inductive-recursive-descriptions/) —
  Larry Diehl, 4 August 2014

<div></div>
</section>
<section id="tab6" class="tab-content" markdown="1">

#### (anti)tech

* [The hardest working font in Manhattan](https://aresluna.org/the-hardest-working-font-in-manhattan/) — Marcin Wichary, 14 February 2025
* [The Subprime AI Crisis](https://www.wheresyoured.at/subprimeai/) — Edward Zitron, 16 September 2024
* [I Will Fucking Piledrive You If You Mention AI Again](https://ludic.mataroa.blog/blog/i-will-fucking-piledrive-you-if-you-mention-ai-again/) — Ludicity, 19 June 2024
* [Why I Lost Faith in Kagi](https://d-shoot.net/kagi.html) — lori, 2 April 2024
* [A guide to potential liability pitfalls for people running a Mastodon instance](https://denise.dreamwidth.org/91757.html) — Denise, 20 November 2022
* [A look at search engines with their own indexes](https://seirdy.one/posts/2021/03/10/search-engines-with-own-indexes/) — Seirdy, 10 March 2021
* [Here is the article you can send to people when they say "But the environmental issues with cryptoart will be solved soon, right?"](https://everestpipkin.medium.com/but-the-environmental-issues-with-cryptoart-1128ef72e6a3) — Everest Pipkin, 3 March 2021
* [Ditherpunk — The article I wish I had about monochrome image dithering](https://surma.dev/things/ditherpunk/index.html) — Surma, 4 January 2021
* [Text Rendering Hates You](https://faultlore.com/blah/text-hates-you/) — Aria Desires, 28 September 2019
* [A Spectre is Haunting Unicode](https://www.dampfkraft.com/ghost-characters.html) — Paul O'Leary McCann, 29 July 2018
* [Return of the Obra Dinn](https://forums.tigsource.com/index.php?topic=40832.msg1363742#msg1363742) (forum post on dithering) — Lucas Pope, 23 November 2017
* [The mystery of the phantom reference](https://harzing.com/publications/white-papers/the-mystery-of-the-phantom-reference) — Anne-Wil Harzing, 26 October 2017
* [A guide to PNG optimization](https://optipng.sourceforge.net/pngtech/optipng.html) — Cosmin Truţa, 10 May 2008

#### medicine

* [How 3M Executives Convinced a Scientist the Forever Chemicals She Found in Human Blood Were Safe](https://www.propublica.org/article/3m-forever-chemicals-pfas-pfos-inside-story) — Sharon Lerner, 20 May 2024
* [Breaking Off My Chemical Romance](https://www.thenation.com/article/society/ssri-antidepressant-side-effects/) — P.E. Moskowitz, 23 March 2022
* [Reverse Engineering the source code of the BioNTech/Pfizer SARS-CoV-2 Vaccine](https://berthub.eu/articles/posts/reverse-engineering-source-code-of-the-biontech-pfizer-vaccine/) — Bert Hubert, 25 December 2020

#### society

* [The Dark Secrets Behind the Neil Gaiman Allegations](https://www.vulture.com/article/neil-gaiman-allegations-controversy-amanda-palmer-sandman-madoc.html) — Lila Shapiro, 13 January 2025
* [ACM Profits Considered Harmful](https://cacm.acm.org/opinion/acm-profits-considered-harmful/) — William J. Bowman, 25 July 2024
* [The Syncophant](https://newleftreview.org/sidecar/posts/the-sycophant) — Lorna Finlayson, 21 September 2023
* [Why Legal Immigration is Nearly Impossible](https://www.cato.org/policy-analysis/why-legal-immigration-nearly-impossible) — David J. Bier, 13 June 2023
* [So you want to apologize... now what?](https://sfconservancy.org/blog/2021/apr/20/how-to-apologize/) — Sage A. Sharp, 20 April 2021
* [Wealth, shown to scale](https://mkorostoff.github.io/1-pixel-wealth/) — Matt Korostoff, 3 April 2021
* [Everyone Is Beautiful and No One Is Horny](https://bloodknife.com/everyone-beautiful-no-one-horny/) — Raquel S. Benedict, 14 February 2021

#### other

* [Coffee in Vancouver](https://homes.cs.washington.edu/~jmsy/blog/coffee-in-vancouver.html) — James Yoo, 24 August 2023
* [How Millennials Killed Mayonnaise](https://www.phillymag.com/news/2018/08/11/mayonnaise-industry-millennials/) — Sandy Hingston, 11 August 2018
* [Things I Won't Work With](https://www.science.org/topic/blog-category/things-i-wont-work-with), Derek Lowe, series

</section>
</div>